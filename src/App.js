import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import AddPost from "./components/AddContact";
import EditContact from "./components/EditContact";
import Table from "./components/Table";
// import Navbar from "./components/Navbar";
import "./styles.css";

import Login from './components/Login/Login';
import Home from './components/Home/Home';
import MainHeader from './components/MainHeader/MainHeader';

const App = () => {

  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect( () => {
    let storeLoginInfo = sessionStorage.getItem('login');
    // console.log( storeLoginInfo );
    if( storeLoginInfo === 'true' ){
      setIsLoggedIn( true );
    }    
  }, []);

  const loginHandler = (email, password) => {
    // We should of course check email and password
    // But it's just a dummy/ demo anyways
    sessionStorage.setItem('login', true);
    setIsLoggedIn(true);
  };

  const logoutHandler = () => {
    sessionStorage.removeItem('login');
    setIsLoggedIn(false);
  };


  return (
    <div className="App">
      <ToastContainer />
      {/* <Navbar /> */}

      {/* <Route exact path="/" component={() => <Table />} />
      <Route exact path="/add" component={() => <AddPost />} />
      <Route exact path="/edit/:id" component={() => <EditContact />} /> */}

      <MainHeader isAuthenticated={isLoggedIn} onLogout={logoutHandler} />
      <main>
        {!isLoggedIn && <Login onLogin={loginHandler} />}
        {isLoggedIn && <Home onLogout={logoutHandler} />}
      </main>

    </div>
  );
};
export default App;
