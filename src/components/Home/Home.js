import React from 'react';

import Card from '../UI/Card/Card';
import classes from './Home.module.css';

import { Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import AddPost from "../AddContact";
import EditContact from "../EditContact";
import Table from "../Table";

const Home = (props) => {
  return (
    <Card className={classes.home}>
      <Route exact path="/" component={() => <Table />} />
      <Route exact path="/add" component={() => <AddPost />} />
      <Route exact path="/edit/:id" component={() => <EditContact />} />
      {/* <h1>Welcome back!</h1> */}
    </Card>
  );
};

export default Home;
